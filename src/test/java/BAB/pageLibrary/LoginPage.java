package BAB.pageLibrary;

import org.openqa.selenium.WebElement;

import BAB.testBase.TestBase;

public class LoginPage extends TestBase {

	public void loginToApplication() throws Exception {
		
		WebElement userEmail = getWebElement("bab.login.userEmail");
		System.out.println("entering userName to the userName textbox");	
		userEmail.sendKeys(Repository.getProperty("userEmail"));
		
		WebElement password = getWebElement("actiTime.login.password");		
		System.out.println("entering password to the password textbox");
		password.sendKeys(Repository.getProperty("password"));
		
		System.out.println("clicking on login button");
		getWebElement("bab.login.loginButton").click();
	}
}