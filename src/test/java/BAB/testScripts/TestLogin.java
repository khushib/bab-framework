package BAB.testScripts;

import java.io.IOException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import BAB.pageLibrary.LoginPage;
import BAB.testBase.TestBase;

public class TestLogin extends TestBase {

	@BeforeClass
	public void setUp() throws IOException {
		init();
	}

	@Test
	public void testLogin() throws Exception {

		LoginPage login = new LoginPage();
		login.loginToApplication();
	}
}